# StateCHart VIsualiZation (SCHVIZ)

SCHVIZ is a React Component that accepts SCXML documents as input and generates
interactive, hierarchical SVG graph diagrams as output. Pronounced like
__schvitz__,  which is Yiddish for "sweat". 

You can find an example of its use in a ReactJS application [here](https://gitlab.com/scion-scxml/schviz/tree/master/test-integration).

You can find an example of its use in a VanillaJS context [here](https://gitlab.com/scion-scxml/schviz/blob/master/test-integration/vanilla-example.html).
